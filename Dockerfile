FROM centos:7

RUN yum -y update && yum -y install python3 python3-pip
RUN pip3 install flask flask_restful flask_jsonpify

ADD python-api.py /python-api/

EXPOSE 5290

ENTRYPOINT ["python3"]
CMD ["/python-api/python-api.py"]